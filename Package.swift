// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "AlertsPickers",
    platforms: [.iOS(.v13)],
    products: [
        .library(name: "AlertsPickers", targets: ["AlertsPickers"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "AlertsPickers",
            dependencies: [],
            sources: ["Source/"]
        ),
    ],
    swiftLanguageVersions: [.v5]
)
